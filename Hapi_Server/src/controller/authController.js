const modeluser = require('../../model/photoAppModel')
const user = modeluser.userModel


exports.postRegist = async (request, h) => {                          
    const maxId = await user.aggregate([{$group:{_id:"maxId", max:{$max: "$id"}}}])
    const newId = maxId[0].max + 1
    const email = request.payload.email
    const checkEmail = await user.findOne({ email: email }).lean()
        if(!checkEmail){
            Object.assign(request.payload, {id : newId})
            newUser = await user.insertMany([request.payload])
            h.response().code(201)
            return newUser
        }
        return h.response({message : 'email is already exist'}).code(404)
}

exports.postLogin = async (request, h) => {                        
    const payload=request.payload
    const login =await user.findOne(payload).lean()
    if(!login){
        return h.response({message: 'something wrong with your email and password'}).code(404)
    }
    return h.response(login).code(200)
}
