const modeluser = require('../../model/photoAppModel')
const modelimages = require('../../model/imageCollectionModel')
const fs = require('fs')


const user = modeluser.userModel
const images = modelimages.imagesModel

exports.rootPath = async (request, h) => {
    return h.file('index.html')
}


exports.getUsers = async (request, h) => {
    return user.find({})
}

exports.postLogin = async (request, h) => {                        
    const payload=request.payload
    const login =await user.findOne(payload).lean()
    if(!login){
        return h.response({message: 'something wrong'}).code(404)
    }
    return h.response(login).code(200)
}



exports.postRegist = async (request, h) => {                          
    const maxId = await user.aggregate([{$group:{_id:"maxId", max:{$max: "$id"}}}])
    const newId = maxId[0].max + 1
    const email = request.payload.email
    const checkEmail = await user.findOne({ email: email }).lean()
        if(!checkEmail){
            Object.assign(request.payload, {id : newId})
            newUser = await user.insertMany([request.payload])
            h.response().code(201)
            return newUser
        }
        return h.response({message : 'email is already exist'}).code(404)
}

exports.PUTafterRegist = async (request, h) => {
    const email = request.params.email
    const timestamp = new Date().getTime();
    const dir = `./public/img/${email}/avatar/`
    const fileName = `${timestamp}.jpg`
    const path = dir + fileName
    const { img, firstname, lastname, username } = request.payload
    try {
        // create directory for images if not exist
        !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true })

        fs.writeFile(path, img, "base64", function (err) {
            console.log(err)
            return img
        });
        const update = await user.findOneAndUpdate({ email: email }, { firstname: firstname, lastname: lastname, username: username, avatar: path })
        return update
    } catch (err) {
        throw new Error(err)
    }
}


exports.PostPicture = async (request, h) => {
    const { email ,img, created_at } = request.payload;

    const timestamp = new Date().getTime();
    const dir = `./public/img/${email}/postedImages/`
    const fileName = `${timestamp}.jpg`
    const path = dir + fileName

    try {
        !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true })
        fs.writeFile(path, img, "base64", function (err) {
            console.log(err)
            return img
        });

        newImages = await images.insertMany([request.payload])
        h.response().code(201)
        return newImages

    } catch (error) {
        return error
    }


}

