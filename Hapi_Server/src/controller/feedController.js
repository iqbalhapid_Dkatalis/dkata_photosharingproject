const modeluser = require('../../model/photoAppModel')
const modelimages = require('../../model/imageCollectionModel')
const fs = require('fs')


const user = modeluser.userModel
const images = modelimages.imagesModel


exports.showFeed = async (request, h) => {
    try {
        const email = request.params.email
        const result = await images.find({'email' : email})
        return h.response(result).code(200)
    }catch{
        return h.response({message : 'whoops there is an error'}).code(400)
    }
    
}

exports.newFeed = async (request, h) => {
    const getEmail = request.params.email
    const email = await user.findOne({'email' : getEmail}).lean()
    const img = request.payload.img
    
    if(getEmail == email.email){
    try{
            const timestamp = new Date().getTime();
            const dir = `./public/img/${email.email}/posting/`
            const fileName = `${timestamp}.jpg`
            const path = dir + fileName

            const newPost = new images({
                email: email.email,
                caption: request.payload.caption,
                created_at: new Date().toISOString(),
                updated_at: new Date().toISOString(),
                url : path
            })

            await images.insertMany([newPost])
            
            !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true })    
            fs.writeFile(path, img, 'base64', function(err) {
                if(err) {
                    throw new Error(err)
                }
            });
            return h.response({message: 'Your photo succesfully posted'}).code(201)
        } catch (err) {
            return h.response(err).code(400)
        }
    }else{
        return h.response({ error: 'whoop there is an error'})
    }

}
