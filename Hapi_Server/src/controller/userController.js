const modeluser = require('../../model/photoAppModel')
const modelimages = require('../../model/imageCollectionModel')
const fs = require('fs')


const user = modeluser.userModel
const images = modelimages.imagesModel


exports.rootPath = async (request, h) => {
    return h.file('index.html')
}

exports.getUsers = async (request, h) => {
    return user.find({})
}

exports.PUTafterRegist = async (request, h) => {
    const email = request.params.email
    const timestamp = new Date().getTime();
    const dir = `./public/img/${email}/avatar/`
    const fileName = `${timestamp}.jpg`
    const path = dir + fileName
    const { img, firstname, lastname, username } = request.payload
    try {
        // create directory for images if not exist
        !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true })

        fs.writeFile(path, img, "base64", function (err) {
            console.log(err)
            return img
        });
        const update = await user.findOneAndUpdate({ email: email }, 
            { firstname: firstname, lastname: lastname, username: username, avatar: path })
        return update
    } catch (err) {
        throw new Error(err)
    }
}


exports.PostPicture = async (request, h) => {
    const { email, img, created_at } = request.payload;

    const timestamp = new Date().getTime();
    const dir = `./public/img/${email}/postedImages/`
    const fileName = `${timestamp}.jpg`
    const path = dir + fileName

    try {
        !fs.existsSync(dir) && fs.mkdirSync(dir, { recursive: true })
        fs.writeFile(path, img, "base64", function (err) {
            console.log(err)
            return img
        });

        newImages = await images.insertMany([request.payload])
        h.response().code(201)
        return newImages

    } catch (error) {
        return error
    }
}

