'use strict'
const Path = require('path')
const Hapi = require('@hapi/hapi')
const HapiSwagger = require('hapi-swagger')

const {connectMongo} = require('./connectMongo')
const route = require('../routes/mainRoutes')


const server = new Hapi.server({
    port: 3000,
    host: '0.0.0.0',
    routes : {
        files : {
            relativeTo : Path.join(__dirname, '../public')
        }
    }
})

//swagger for documentation
const swaggerOptions = {
    info: {
        title: 'PhotoApp API Documentation',
        version: '0.0.1'
    }
}

//connectMongo
connectMongo()


//server method func related


//validation


const start = async () => {
    //all config server code write here
    await server.register([
        require('inert'),
        require('vision'),
        {
            plugin : HapiSwagger,
            options : swaggerOptions
        },
        require('@hapi/inert')
    ])
    server.route(route)
    await server.start()
    console.log('Server running at: ', server.info.uri)
    return server
}


const init = async () => {
    await server.initialize();
    return server;
}

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
})


exports.start = start;
exports.init = init;