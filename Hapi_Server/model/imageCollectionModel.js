const Mongooose = require('mongoose')
const { Schema } = Mongooose

const imgSchema = new Schema({
    id : Number,
    email : String,
    img: String,
    url : String,
    created_at : {type : Date, default: Date.now},
    caption : String,
    updated_at : {type : Date, default: null },
    likes : Array,
    comments : Array
})

const imagesModel = Mongooose.model('images', imgSchema);
module.exports = { imagesModel }