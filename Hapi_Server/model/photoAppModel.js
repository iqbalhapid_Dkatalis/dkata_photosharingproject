const Mongooose = require('mongoose')
const { Schema } = Mongooose

const userSchema = new Schema({
    id : Number,
    firstname: String,
    lastname : String,
    username: String,
    email : String,
    password : String,
    avatar: String,
    img: String,
    created_at : {type : Date, default: Date.now},
    updated_at : {type : Date, default: null},
})

const userModel = Mongooose.model('photo', userSchema);

module.exports = { userModel }