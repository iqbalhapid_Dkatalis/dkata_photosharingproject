'use strict'
const feedController = require('../src/controller/feedController')
const Joi = require('@hapi/joi')

const basepath = '/api/v1'


module.exports = [
    {
        method: 'GET',
        path: basepath + '/PhotoApp/Post/{email}',
        handler: feedController.showFeed
    },
    {
        method: 'POST',
        path: basepath + '/PhotoApp/newPost/{email}',
        handler: feedController.newFeed
    },
    {
        method: 'GET',
        path: basepath + '/PhotoApp/getPost/{email}/posting/{filename}',
        options: {
            auth : null
        },
        handler: (request, h) => {
            return h.file(`img/${request.params.email}/posting/${request.params.filename}`)
        },
    }
]