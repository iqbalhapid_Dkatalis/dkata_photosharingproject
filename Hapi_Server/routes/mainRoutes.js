const auth = require('./authRouter')
const feed = require('./feedRouter')
const user = require('./userRouter')

module.exports = [].concat(auth, feed, user)