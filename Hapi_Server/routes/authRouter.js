'use strict'
const authController = require('../src/controller/authController')
const Joi = require('@hapi/joi')

const basepath = '/api/v1'

module.exports = [
    {
        method: 'POST',
        path: basepath + '/PhotoApp/register',
        options: {
            description: 'Post new user',
            notes: 'Returns an array of payloads',
            tags: ['api/v1/PhotoApp/register'],
            validate : {
                payload : {
                    email : Joi.string().required(),
                    password : Joi.string().required()
                }
            }
        },
        handler: authController.postRegist
    },
    {
        method: 'POST',
        path: basepath + '/PhotoApp/SignIn',
        options: {
            description: 'Sign in user as valid user',
            notes: 'Returns an array of payloads',
            tags: ['api/v1/PhotoApp/Signin'],
            validate : {
                payload : {
                    email : Joi.string().required(),
                    password : Joi.string().required()
                }
            }
        },
        handler: authController.postLogin
    }
]