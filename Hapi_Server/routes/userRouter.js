'use strict'
const userController = require('../src/controller/userController')
const Joi = require('@hapi/joi')

const basepath = '/api/v1'
module.exports =  [
    {
        method: 'GET',
        path: '/',
        options: {
            description: 'root to document',
            notes: 'a page for redirect to swagger ui',
            tags: ['root'],
        },
        handler: userController.rootPath
    },
    {
        method: 'GET',
        path: basepath + '/PhotoApp',
        options: {
            description: 'returning list all users',
            notes: 'list all users',
            tags: ['api/v1/PhotoApp'],
        },
        handler: userController.getUsers
    },
    {
        method: 'PUT',
        path: basepath + '/PhotoApp/register/{email}',
        options: {
            description: 'Edit and Add some field for user',
            notes: 'Returns an array of payloads',
            tags: ['api/v1/PhotoApp/register/{email}']
        },
        handler: userController.PUTafterRegist
    },
    {
        method: 'POST',
        path: basepath + '/PhotoApp/upload',
        options: {
            description: 'post a picture',
            notes: 'Returns an array of payloads',
            tags: ['api/v1/PhotoApp/upload/{email}'],
            validate : {
                payload : {
                    email : Joi.string().required()
                }
            }
        },
        handler: userController.PostPicture
    }
]
