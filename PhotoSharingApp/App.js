
import React, { Component } from 'react';
import { StatusBar, AppRegistry } from 'react-native';
import Root from './src/routes/Root'
import store from './src//redux/store/store'
import { Provider as ReduxProvider } from 'react-redux'
import { Provider as PaperProvider } from 'react-native-paper';

StatusBar.setBarStyle('light-content', true);


class App extends Component {
  render() {
    return (
      <ReduxProvider store = {store} >
        <PaperProvider>
          <Root />
        </PaperProvider>
      </ReduxProvider>
    );
  }
}

AppRegistry.registerComponent('App', () => App);

export default App;