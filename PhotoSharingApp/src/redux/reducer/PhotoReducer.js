import { combineReducers } from 'redux'
import ACTION_TYPES from '../action/ActionTypes'

const initialState = {
    data: '',
    isLoading: false,
    error: '',
    email: '',
    userData: {}
}

const apiReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TYPES.GET_DATA_SUCCESS:
            return {
                ...state,
                data: action.data
            }
        case ACTION_TYPES.GET_DATA_USER:
            return {
                ...state,
                userData: action.data
            }
        case ACTION_TYPES.POST_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload,
                isLoading: true,
                error: action.error
            }
        case ACTION_TYPES.PUT_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload,
                isLoading: true
            }
        case ACTION_TYPES.DATA_ERROR:
            return {
                ...state,
                error: action.payload.error
            }
        case ACTION_TYPES.AUTHENTICATION:
            return {
                ...state,
                email: action.email
            }
        default:
            return state
    }
}

const appReducers = combineReducers({ apiReducer })
const rootReducer = (state, action) => appReducers(state, action)

export default rootReducer