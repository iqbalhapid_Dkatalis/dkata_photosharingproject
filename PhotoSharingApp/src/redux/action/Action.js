import ACTION_TYPES from './ActionTypes'

export const getDataSuccess = data => ({
    type: ACTION_TYPES.GET_DATA_SUCCESS,
    data: data
})


export const getUserSuccess = data => ({
    type: ACTION_TYPES.GET_USER_SUCCESS,
    data: data
})


export const postDataSuccess = response => ({
    type: ACTION_TYPES.POST_DATA_SUCCESS,
    payload: response
})

export const putDataSuccess = response => ({
    type: ACTION_TYPES.PUT_DATA_SUCCESS,
    payload: response
})

export const authSuccess = email => ({
    type: ACTION_TYPES.AUTHENTICATION,
    email: email
})

export const dataError = error => ({
    type: ACTION_TYPES.DATA_ERROR,
    payload: error
})