
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack'
import LoginScreen from '../screens/login'
import RegisterScreen from '../screens/register'
import ProfileScreen from '../screens/profileCreation'
import FeedScreen from '../screens/feed'
import myProfileScreen from '../screens/myprofile'
import postingImageScreen from '../screens/postingImage'
import color from '../screens/styles/color'


import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs'


const StackNavigation = createStackNavigator(
    {
        Login: LoginScreen,
        Register: RegisterScreen,
        CreateProfile : ProfileScreen
    },
    {
        initialRouteName: 'Login'
    }
);

const BottomNavigation = createMaterialBottomTabNavigator(
    {
        Feed : FeedScreen,
        Posting : postingImageScreen,
        MyProfile : myProfileScreen
    },
    {
        initialRouteName: 'Feed',
        activeColor: color.darkOrange,
        inactiveColor: color.white,
        barStyle: { backgroundColor: color.green02, opacity: 0.5 },
    }
)



const SwitchNavigation = createSwitchNavigator(
    {
        StackNavigation,
        BottomNavigation
    },
    {
        initialRouteName : 'StackNavigation'
    }
)

const Root = createAppContainer(SwitchNavigation);

export default Root