import React, {Component} from 'react'
import {StyleSheet} from 'react-native'
import color from './color'

const styles = StyleSheet.create({
    wrapper : {
        backgroundColor: color.green01,height: '100%'
    },
    container : {
        marginHorizontal: 12, marginTop: 50
    },
    contentWrap : {
        marginVertical: 10, alignSelf: "center"
    },
    RegistHeader: {
        fontSize: 20,
        color: color.white,
        fontWeight: '300',
        marginBottom: 15,
        marginTop: 12
    },
    text : {
        fontSize : 12,width: '100%', height: 40, borderBottomWidth: 2, borderBottomColor: color.green02
    },
    BtnSubmit : {
        width: 100, height: 30,marginLeft: 5, marginTop: 12, marginBottom: 5
    },
    BtnText : {
        alignSelf: 'center', color: 'white', marginVertical: 2
    }
})

export default styles