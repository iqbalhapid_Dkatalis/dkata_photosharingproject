
import { StyleSheet } from 'react-native';
import colors from '../styles/color';

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor: colors.blue,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    },
    content: {
        paddingTop: 20,
    },
    closeButton: {
        position: 'absolute',
        left: 20,
        zIndex: 9999,
    },
    headerStyle: {
        backgroundColor: colors.white,
        borderBottomWidth: 0,
    },
    heading: {
        alignSelf: "center",
        fontSize: 28,
        fontWeight: '800',
        color: colors.white,
        paddingLeft: 20,
        paddingRight: 20,
        marginTop: 15,
    },
    privacyOptions: {
        marginTop: 40,
    },
    img : {
        alignSelf: 'center',
        width:150,
        height: 150,
        borderRadius: 75,
        borderWidth: 5,
        borderColor: colors.white
    },
    privacyHeading: {
        fontSize: 16,
        fontWeight: '400',
        color: colors.white,
        marginBottom: 5,
        paddingLeft: 20,
        paddingRight: 20,
    },
    privacyOptionItem: {
        flex: 1,
        padding: 20,
    },
    privacyOptionTitle: {
        fontSize: 16,
        fontWeight: '400',
        color: colors.white,
    },
    privacyOptionDescription: {
        fontSize: 14,
        fontWeight: '200',
        color: colors.white,
        marginTop: 10,
        paddingRight: 90,
    },
    privacyRadioInput: {
        position: 'absolute',
        top: 0,
        right: 0,
    },
    inputWrapper: {
        paddingLeft: 20,
        paddingRight: 20,
    },
    divider: {
        borderBottomWidth: 1,
        borderBottomColor: colors.gray06,
        height: 1,
        flex: 1,
        marginLeft: 20,
        marginRight: 20,
    },
    createButton: {
        position: 'absolute',
        bottom: 0,
        right: 10,
        width: 110,
    },
    buttonIcon: {
        position: 'absolute',
        right: 0,
        top: '50%',
        marginTop: -16,
    },
});

export default styles;
