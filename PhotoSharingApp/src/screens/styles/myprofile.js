import {StyleSheet} from 'react-native'
                
const styles = StyleSheet.create({
    container : {
        flexDirection: 'row',
        margin: 10,
    },
    surface: {
        marginHorizontal: 10,
        padding: 8,
        height: 150,
        width: 150,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4,
        borderRadius: 20
    },
});

export default styles;