import {StyleSheet} from 'react-native' 
import color from './color/index'

const styles = StyleSheet.create({
    imageWrap : {
        backgroundColor: color.green01,
        width: '60%',
        height: '60%',
        borderRadius: 20,
        borderWidth: 5,
        borderColor: color.green02,
        marginBottom: 50
    },
    img :{
        width: '100%',
        height: '100%',
        borderRadius: 20
    },
    caption : {
        width: '100%',
        height: 55,
        position: 'absolute',
        alignSelf: 'center',
        backgroundColor: color.gray05
    },
    wrapcaption: {
        marginBottom: 80
    },
    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: color.gray05,
    },

    TouchableOpacityStyle: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 10,
        bottom: 10,
    },

    FloatingButtonStyle: {
        width: 85,
        height: 85
    },
})

export default styles