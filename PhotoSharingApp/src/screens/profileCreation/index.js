import React, { Component } from 'react'
import { Text, View, TouchableHighlight, TouchableOpacity, ScrollView, Image} from 'react-native'
import Axios from 'axios'
import ImagePicker from 'react-native-image-picker'
import { TextInput } from 'react-native-paper';

import Icon from 'react-native-vector-icons/Ionicons'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import RoundedButton from '../../components/buttons/RoundedButton'
import RadioInput from '../../components/form/RadioInput'
import styles from '../styles/profileCreation'
import colors from '../styles/color'


class ProfileCreation extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: (
            <TouchableOpacity
                style={styles.closeButton}
                onPress={() => navigation.goBack()}
            >
                <Icon
                    name="md-close"
                    size={30}
                    color={colors.lightBlack}
                />
            </TouchableOpacity>
        ),
        headerStyle: styles.headerStyle,
    });

    constructor(props) {
        super(props)
        this.state = {
            user : {
            id : this.props.navigation.state.params.itemId,
            email: this.props.navigation.state.params.itemEmail,
            firstname: '',
            lastname: '',
            username: '',
            avatar: '',
            img : '',
            },
            loading: false,
        };
    }

    _handleImgPick = () => {
        const options = {
            title: 'Select Avatar',
            maxWidth: 500,
            maxHeight : 500,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                const source ={ uri :response.uri };
                const path = response.path
                const data = response.data;

                this.setState({
                    avatar : source,
                    user : {...this.state.user, avatar: source },
                    user : {...this.state.user, img : data}
                });
            }
        });
    }

    handleIcon = () => {
        if(this.state.avatar){
            return (
                <Image style={styles.img} source={this.state.avatar} />
            )}{
            return (
                <Icon
                        name="ios-contact"
                        size={180}
                        color={colors.white}
                    />
            )
            }
    }
    

    _handleCreateProfile = async () => {
        try {
            const url = `http://192.168.133.2:3000/api/v1/PhotoApp/register/${this.state.user.email}`
            const update = await Axios.put(url, {
                firstname : this.state.user.firstname,
                lastname: this.state.user.lastname,
                username : this.state.user.username,
                img : this.state.user.img
            })
            //return update
            if(update){
                this.props.navigation.navigate('Login')
            }
        } catch (error) {
            return error;
        }
    }

    render() {
        console.log(this.state.user.img)
        return (
            <View style={styles.wrapper}>
                <ScrollView style={styles.scrollView}>
                    <Text style={styles.heading}>
                        Create Your Profile
                    </Text>
                    <TouchableOpacity onPress={this._handleImgPick}>
                    <View style={{ alignItems:'center', marginTop : 20}}>
                    {this.handleIcon()}
                    </View>
                    </TouchableOpacity>
                    <View style={styles.content}>
                        <View style={styles.inputWrapper}>
                            <TextInput
                                mode = 'outlined'
                                theme ={{ colors: { background: colors.blue, placeholder: colors.white, text: colors.white } }}
                                label='Firstname'
                                value={this.state.text}
                                onChangeText={text => this.setState({
                                    user: { ...this.state.user, firstname: text }
                                })}
                            />
                            <View style={{marginVertical: 20}}>
                            <TextInput
                                mode = 'outlined'
                                theme ={{ colors: { background: colors.blue, placeholder: colors.white, text: colors.white } }}
                                label='Lastname'
                                value={this.state.text}
                                onChangeText={text => this.setState({
                                    user: { ...this.state.user, lastname: text }
                                })}
                            />
                            </View>
                            <TextInput
                                mode = 'outlined'
                                theme ={{ colors: { background: colors.blue, placeholder: colors.white, text: colors.white } }}
                                label='Username'
                                selectionColor= {colors.green02}
                                value={this.state.text}
                                onChangeText={text => this.setState({
                                    user: { ...this.state.user, username: text }
                                })}
                                style={colors.green02}
                            />
                        </View>
                        <View style={styles.privacyOptions}>
                            <Text style={styles.privacyHeading}>
                                Privacy
                    </Text>
                            <TouchableHighlight
                                // onPress={() => this.selectPrivacyOption('public')}
                                style={styles.privacyOptionItem}
                                underlayColor={colors.gray01}
                            >
                                <View>
                                    <Text style={styles.privacyOptionTitle}>
                                        Public
                                    </Text>
                                    <Text style={styles.privacyOptionDescription}>
                                        Visible to everyone and included on your public profile.
                                    </Text>
                                    <View style={styles.privacyRadioInput}>
                                        <RadioInput
                                            backgroundColor={colors.gray07}
                                            borderColor={colors.gray05}
                                            selectedBackgroundColor={colors.green01}
                                            selectedBorderColor={colors.green01}
                                            iconColor={colors.white}
                                            // selected={privacyOption === 'public'}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>
                            <View style={styles.divider} />
                            <TouchableHighlight
                                // onPress={() => this.selectPrivacyOption('private')}
                                style={styles.privacyOptionItem}
                                underlayColor={colors.gray01}
                            >
                                <View>
                                    <Text style={styles.privacyOptionTitle}>
                                        Private
                                    </Text>
                                    <Text style={styles.privacyOptionDescription}>
                                        Visible only to you and any friends you follow.
                                    </Text>
                                    <View style={styles.privacyRadioInput}>
                                        <RadioInput
                                            backgroundColor={colors.gray07}
                                            borderColor={colors.gray05}
                                            selectedBackgroundColor={colors.green01}
                                            selectedBorderColor={colors.green01}
                                            iconColor={colors.white}
                                            // selected={privacyOption === 'private'}
                                        />
                                    </View>
                                </View>
                            </TouchableHighlight>
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.createButton}>
                    <TouchableOpacity>
                    <RoundedButton
                        text="Create"
                        textColor={colors.white}
                        textAlign="left"
                        background={colors.green01}
                        borderColor="transparent"
                        iconPosition="left"
                        // loading={loading}
                        icon={(
                            <View style={styles.buttonIcon}>
                                <FontAwesomeIcon name="angle-right" color={colors.white} size={30} />
                            </View>
                        )}
                        handleOnPress={() => this._handleCreateProfile()}
                    />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default ProfileCreation