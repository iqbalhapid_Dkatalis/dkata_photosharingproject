import React, {Component} from 'react'
import {Text, View, TouchableOpacity, Image, KeyboardAvoidingView} from 'react-native'
import ImagePicker from 'react-native-image-picker'
import Axios from 'axios'
import styles from '../styles/postingImage'
import Icon from 'react-native-vector-icons/Ionicons'
import color from '../styles/color'
import {TextInput} from 'react-native-paper'
import AsyncStorage from '@react-native-community/async-storage'
import { postData } from '../../redux/action/ActionCreator'
import {connect} from 'react-redux'



class postingImage extends Component {
    static NavigationOptions = {
        header: null
    }
    
    constructor(props){
        super(props)
        this.state = {
                img : '',
                caption : '',
                email: '',
        }
    }

    async componentDidMount() {
        const getEmail = await AsyncStorage.getItem('userLogin')
        this.setState({email :getEmail})
    }

    handlePostImage = async () => {
        const { postData, data, navigation } = this.props
        const { caption, img, email } = this.state

        let payload = {
            caption,
            img
        }

        const url = `http://192.168.133.2:3000/api/v1/PhotoApp/newPost/${email}`
        console.log(email)
        try {
            await postData(url, payload)
            if(data){
                setTimeout(() => {
                    alert(data.message)
                    navigation.navigate('Feed')
                },1000)
            }
        }catch(error){
            alert('Whoops, something wrong')
            console.log(error);
        }
    }

    handleImagePick = () => {
        const options = {
            title: 'Select Images you want to post',
            maxWidth: 500,
            maxHeight : 500,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                const source ={ uri :response.uri };
                const data = response.data;
                this.setState({
                    avatar : source,
                    img : data 
                });
            }
        });
    }

    handleIcon = () => {
        if(this.state.avatar){
            return (
                <Image style={styles.img} source={this.state.avatar} />
            )}{
            return (
                <Icon
                        name="ios-contact"
                        size={250}
                        color='white'
                    />
            )
            }
    }

    render(){
        return(
            <View style={styles.MainContainer}>
                    <View style={styles.imageWrap}>
                            {this.handleIcon()}
                    </View>
                    <TouchableOpacity style={{marginBottom:50}} onPress={() => this.handlePostImage()}>
                                    <View>
                                        <Text>SAVE</Text>
                                    </View>
                    </TouchableOpacity>
                    <View style={styles.wrapcaption}>
                            <TextInput
                                mode = 'outlined'
                                theme ={{ color: { background: color.gray05, placeholder: color.gray05, text: color.white, primary: color
                                    .white } }}
                                label = "Please add Caption"
                                value={ (this.state.caption) }
                                keyboardType='default'

                                style={styles.caption}
                                onChangeText={text => this.setState({
                                caption: text })} />
                            </View>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={this.handleImagePick}
                        style={styles.TouchableOpacityStyle}>
                        <Icon 
                        name="md-add-circle"
                        size={80}
                        style={styles.FloatingButtonStyle}
                        color={color.darkOrange}
                        />
                    </TouchableOpacity>
                </View>
        )
    }
}

const mapStateToProps = state => ({
    data : state.apiReducer.data
})

const mapDispatchToProps = dispatch => ({
        postData: (url, payload) => dispatch(postData(url, payload))
})
    

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(postingImage)
