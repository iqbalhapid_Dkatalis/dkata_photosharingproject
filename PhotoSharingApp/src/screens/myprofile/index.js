import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

const logout = [
    {
        title: 'Logout',
        icon: 'log-out',
        ScreenName: 'Home',
    },
]

export default class Setting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
        }
    }

    _onPressLogout() {
        const { navigation } = this.props
        AsyncStorage.setItem('userLogin', this.state.email)
        setTimeout(() => {
            navigation.navigate('Logout');
            alert('You have successfully logged out')
            this.props.navigation.navigate('Login')
        }, 1000);
    }

    render() {
        return (
            <>
                <ScrollView>
                    <View style={{width: '100%', height: 40, backgroundColor: 'lightgray' }}>
                        <TouchableOpacity onPress={ () => this._onPressLogout()}>
                            <Text style={{alignSelf:'center'}}>Log out</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </>
        )
    }
}