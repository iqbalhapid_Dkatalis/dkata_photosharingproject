import React, { Component } from 'react'
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    KeyboardAvoidingView,
} from 'react-native';

import md5 from 'md5'
import colors from '../styles/color'
import transparentHeaderStyle from '../styles/Navigation'
import InputField from '../../components/form/InputField'
import NextArrowButton from '../../components/buttons/NextArrowButton'
import NavBarButton from '../../components/buttons/NavBarButton'
import styles from '../styles/loginStyle'
import Axios from 'axios'
import { connect } from 'react-redux'
import { authEmail } from '../../redux/action/ActionCreator'
import AsyncStorage from '@react-native-community/async-storage'

class Login extends Component {
    static navigationOptions = ({ navigation }) => ({
        headerRight: <NavBarButton
            handleButtonPress={() => navigation.navigate('ForgotPassword')}
            location="right"
            color={colors.white}
            text="Forgot Password"
        />,
        headerStyle: transparentHeaderStyle,
        headerTransparent: true,
        headerTintColor: colors.white,
    });

    constructor(props) {
        super(props);
        this.state = {
            formValid: true,
            validEmail: false,
            emailAddress: '',
            password: '',
            validPassword: false,
            loadingVisible: false,
            isLogin : false
        };
    }

    async componentDidMount (){
        const checkAuth = await AsyncStorage.getItem('userLogin')
        if(checkAuth) {
            this.props.navigation.navigate('Feed')
        } else {
            this.props.navigation.navigate('Login')
        }
    }


    handleNextButton = () => {
        this.setState({ loadingVisible: true }); 
    }

    handleEmailChange = (email) => {
        const emailCheckRegex = /@RealUniversity.com\b/gi;
        const { validEmail } = this.state;
        this.setState({ emailAddress: email });
        if (!validEmail) {
            if (emailCheckRegex.test(email)) {
                this.setState({ validEmail: true });
            }
        } else if (!emailCheckRegex.test(email)) {
            this.setState({ validEmail: false });
        }
    }


    handlePasswordChange = (password) => {
        const { validPassword } = this.state;
        this.setState({ password });
        if (!validPassword) {
            if (password.length > 4) {
                // Password has to be at least 4 characters long
                this.setState({ validPassword: true });
            }
        } else if (password <= 4) {
            this.setState({ validPassword: false });
        }
    }

    toggleNextButtonState = () => {
        const { validEmail, validPassword } = this.state;
        if (validEmail && validPassword) {
            return false;
        }
        return true;
    }

        
        
    handleSubmit = async () => {
        const url = `http://192.168.133.2:3000/api/v1/PhotoApp/SignIn`
        const data = {
            email : this.state.emailAddress,
            password : md5(this.state.password)
        }
        try{
            const login =  await Axios.post(url, data)
            const res = await login.data
            if(login){
                this.setState({
                    isLogin : true
                })
                await AsyncStorage.setItem('userLogin', this.state.emailAddress);
                this.props.authEmail(this.state.emailAddress)
                alert('welcome') 
                setTimeout( ()=> {
                    this.props.navigation.navigate('Feed')
                },1000 );
        }
        }catch(error){
            alert('check your username & password')
        }
    }

    render() {
        const {
            formValid, loadingVisible, validEmail, validPassword,
        } = this.state;
        const showNotification = !formValid;
        const background = formValid ? colors.green01 : colors.darkOrange;
        const notificationMarginTop = showNotification ? 10 : 0;
        return (
            <KeyboardAvoidingView
                style={[{ backgroundColor: background }, styles.wrapper]}
                behavior="padding"
            >
                <View style={styles.scrollViewWrapper}>
                    <ScrollView style={styles.scrollView}>
                        <Text style={styles.loginHeader}>
                            Log In
            </Text>
                        <InputField
                            labelText="EMAIL ADDRESS"
                            labelTextSize={14}
                            labelColor={colors.white}
                            textColor={colors.white}
                            borderBottomColor={colors.white}
                            inputType="email"
                            customStyle={{ marginBottom: 30 }}
                            onChangeText={this.handleEmailChange}
                            showCheckmark={validEmail}
                            autoFocus
                        />
                        <InputField
                            labelText="PASSWORD"
                            labelTextSize={14}
                            labelColor={colors.white}
                            textColor={colors.white}
                            borderBottomColor={colors.white}
                            inputType="password"
                            customStyle={{ marginBottom: 30 }}
                            onChangeText={this.handlePasswordChange}
                            showCheckmark={validPassword}
                        />
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                        <View style={{padding: 15, backgroundColor: colors.green02, alignItems: 'center', borderRadius: 20}}>
                            <Text style={{fontSize: 16, color: 'white', fontWeight: 'bold'}}>Register</Text>
                        </View>
                        </TouchableOpacity>
                    </ScrollView>
                    <TouchableOpacity onPress={ () => this.handleSubmit()}>
                    <NextArrowButton
                        handleNextButton={() => this.handleSubmit()}
                        disabled={this.toggleNextButtonState()}
                    />
                    </TouchableOpacity>
                </View>
                <View style={[styles.notificationWrapper, { marginTop: notificationMarginTop }]}>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        authEmail : (email) => {
            dispatch(authEmail(email))
        }
    }
}

const mapStateToProps = state => ({
    email : state.apiReducer.email
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)
