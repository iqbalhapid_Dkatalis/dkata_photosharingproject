import React, { Component } from 'react'
import { Text, View, ScrollView, Button} from 'react-native'
import { TextInput } from 'react-native-paper';
import styles from '../styles/registerStyle'
import colors from '../styles/color'
import Axios from 'axios'
import NavBarButton from '../../components/buttons/NavBarButton'
import transparentHeaderStyle from '../../screens/styles/Navigation'
import md5 from 'md5'


class Register extends Component {
    constructor(props){
        super(props)

        this.state = {
            user : {
                id: '',
                email : '',
                password : '',
                retype : ''
            }
        }
    }

    static navigationOptions = () => ({
        headerRight: <NavBarButton
            location="right"
            color={colors.white}
            text="Register Page"
        />,
        headerStyle: transparentHeaderStyle,
        headerTransparent: true,
        headerTintColor: colors.white,
    });

    canBeSubmitted() {
        const {email, password, retype } = this.state.user;
        return (
            password !== retype || password === "" || email === "" || !email.match(/@RealUniversity.com\b/gi)
        );
    }

    handleSubmit = async () => {
        const url = 'http://192.168.133.2:3000/api/v1/PhotoApp/register'
        try {
        const postRegist = await Axios.post(url, {
            email: this.state.user.email,
            password: md5(this.state.user.password)
        })
        const data = postRegist.data
        if(postRegist){
            alert('congratulation your account was created')
            this.props.navigation.navigate('CreateProfile',{ itemId: data[0].id, itemEmail : data[0].email })
        }
        }catch(error){
            alert('email is already used')
            console.log(error);
        }
    }

    render(){
        const isEnabled = this.canBeSubmitted();
        return(
        <ScrollView style={styles.wrapper}>
            <View style={styles.container}>
                <View>
                    <Text style={styles.RegistHeader}>Email</Text>
                    <TextInput
                                mode = 'flat'
                                theme ={{ colors: { background: colors.green02, placeholder: colors.white, text: colors.white, primary: colors
                                .white } }}          
                                label = "email with registered domain"
                                value={ (this.state.user.email) }
                                keyboardType='default'
                                onChangeText={text => this.setState({
                                    user: {...this.state.user, email: text }} )} />
                    <Text style={styles.RegistHeader}>Password</Text>
                    <TextInput
                                mode = 'flat'
                                theme ={{ colors: { background: colors.green02, placeholder: colors.white, text: colors.white, primary: colors
                                    .white } }}
                                label = "Your Password"
                                value={ (this.state.user.password) }
                                keyboardType='default'
                                secureTextEntry={true}
                                onChangeText={text => this.setState({
                                    user: {...this.state.user, password: text }} )} />
                    <Text style={styles.RegistHeader}>Confirmation Password</Text>
                    <TextInput
                                mode = 'flat'
                                theme ={{ colors: { background: colors.green02, placeholder: colors.white, text: colors.white, primary: colors
                                    .white } }}
                                label = "Confirm Your Password"
                                value={ (this.state.user.retype) }
                                keyboardType='default'
                                secureTextEntry={true}
                                onChangeText={text => this.setState({
                                    user: {...this.state.user, retype: text }} )} />
                </View>
                <View style={styles.BtnSubmit}>
                {/* <Button icon="camera" disabled={isEnabled} onPress={() => this.handleSubmit() } mode="contained" /> */}
                    <Button title='submit' disabled={isEnabled} onPress={() => this.handleSubmit() } style={styles.BtnSubmit} />
                </View>
                
            </View>
        </ScrollView>
        )
    }
}

export default Register
