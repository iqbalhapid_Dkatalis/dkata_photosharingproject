import React, {Component} from 'react'
import {View,Text, ScrollView, FlatList, ActivityIndicator} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import { Avatar, Button, Card, Title, Paragraph} from 'react-native-paper'
import { connect } from 'react-redux'
import { ListItem } from 'react-native-elements'
import { getData } from '../../redux/action/ActionCreator'
import AsyncStorage from '@react-native-community/async-storage'

class Feed extends React.Component {
    static NavigationOptions = {
        tabBarLabel:'Home',  
        tabBarIcon: ({ tintColor }) => (  
            <View>  
                <Icon style={[{color: tintColor}]} size={25} name={'home'}/>  
            </View>)
        
    }
    constructor(props){
        super(props)
        this.state = {
            email : ''
        }
    }
    
    async componentDidMount () {
        const { getData } = this.props
        let accessToken = await AsyncStorage.getItem('userLogin');
            this.setState({ 
                email : accessToken
            })
        const url = `http://192.168.133.2:3000/api/v1/PhotoApp/Post/${this.state.email}`
        console.log('EEE - '+ this.state.email)
        if (accessToken) {
        await  getData(url)
        } else {
            this.props.navigation.navigate('Login')
        }
            // alert("something error")
    }


    // keyExtractor = (item, index) => index.toString()

    // renderItem = ({ item }) => (
    // <ListItem
    //     title={item.email}
    //     subtitle={item.email}
    //     bottomDivider
    // />
    // )


    render() {
        const {data} = this.props
        console.log(data)
        // const uri = data[0].url.substr(13)
        // const imgUri = `http://192.168.133.2:3000/api/v1/PhotoApp/getPost/${uri}`
        return(
            <View>
                <ScrollView>
                {data ? <View>
                            <View style={{ marginTop: 20 }}>
                                <Card>
                                    <Card.Title title={`@${data[0].email.substr(0,12)}`} subtitle={data[0].email} left={(props) => <Avatar.Icon {...props} icon="account" />} />
                                    <Card.Content>
                                        <Title>My Title</Title>
                                    <Paragraph>{data[0].caption}</Paragraph>
                                    </Card.Content>
                                    <Card.Cover source={{ uri: `http://192.168.133.2:3000/api/v1/PhotoApp/getPost/${data[0].url.substr(13)}` }} height='100%' />
                                    <Card.Actions>
                                        <Button>Likes</Button>
                                        <Button>comments</Button>
                                    </Card.Actions>
                                </Card>
                            </View>
                        </View>  : <ActivityIndicator/> }
                {/* <FlatList
                keyExtractor={this.keyExtractor}
                data={(data)}
                renderItem={this.renderItem}
                /> */}
            
                    {/* <View>
                        <View style={{ marginTop: 20 }}>
                            <Card>
                                <Card.Title title={`@${data.email.substr(12)}`} subtitle={data.email} left={(props) => <Avatar.Icon {...props} icon="account" />} />
                                <Card.Content>
                                    <Title>Forest Woods</Title>
                                <Paragraph>{data.caption}</Paragraph>
                                </Card.Content>
                                <Card.Cover source={{ uri: imgUri }} height='100%' />
                                <Card.Actions>
                                    <Button>Likes</Button>
                                    <Button>comments</Button>
                                </Card.Actions>
                            </Card>
                        </View>
                    </View> */}
                    <Card>
                        <Card.Title title="Welcome New Member" subtitle="welcome@realuniversity.com" left={(props) => <Avatar.Icon {...props} icon="folder" />} />
                        <Card.Content>
                            <Title>Let's take a look</Title>
                            <Paragraph>You can Post any Picture you like</Paragraph>
                        </Card.Content>
                        <Card.Cover source={{ uri: 'https://picsum.photos/699' }} />
                        <Card.Actions>
                            <Button>likes</Button>
                            <Button>comments</Button>
                        </Card.Actions>
                    </Card>
                    <Card>
                        <Card.Title title="Let's Explore and take a look" subtitle="welcome@realuniversity.com" left={(props) => <Avatar.Icon {...props} icon="folder" />} />
                        <Card.Content>
                            <Title>Will be your Title</Title>
                            <Paragraph>will be your Caption</Paragraph>
                        </Card.Content>
                        <Card.Cover source={{ uri: 'https://picsum.photos/700' }} />
                        <Card.Actions>
                            <Button>likes</Button>
                            <Button>comments</Button>
                        </Card.Actions>
                    </Card>
                </ScrollView>
            </View>
        )
    }
}


const mapStateToProps = state => ({
    email: state.apiReducer.email,
    data: state.apiReducer.data,
});

const mapDispatchToProps = dispatch => ({
    getData: url => dispatch(getData(url))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Feed);