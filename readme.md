### Read me here



# Config and How to Install

`$ git clone this repo`
`$ npm install`
`$ look at the config folder on HapiServer`
`$ set into your machine`

 
####FEATURES
                
+ REST-API Included on this repo (using nodeJS with Hapijs framework)
+ Using some open-source Material (fancy stuff) 
    + Bottom material navigation
    + Card view from Paper
+ Redux state management
+ Async Storage state storage
+ handler Validation on email and hashing password


####ScreenShoot

Image:

![](https://i.imgsafe.org/04/04c6cc03ee.png)

> Login

Image:

![](https://i.imgsafe.org/04/04c76d0ce3.png)

> Register


image :

![](https://i.imgsafe.org/04/04dfc3d704.png)
> Create Profile

image : 
![](https://i.imgsafe.org/04/04dfbce68f.png)

>Welcome Screen

image:

![](https://i.imgsafe.org/04/04dfb4be24.png)

>First Post

image : 
![](https://i.imgsafe.org/04/04dfedfc40.png)
>Your Post


image : 
![](https://i.imgsafe.org/04/04dfd3e270.png)
>LogOut

##Still under Development and Maintaned
##i will finish the project as soon as possible
